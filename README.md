# Exam Randomizer
### Randomize you student list for exams!
#### Requirements:
  * Python 3
  * GTK+ 3
  * libglade

Just populate students.txt file with names of your students and launch exam_random script.
